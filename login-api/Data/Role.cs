﻿using System;
using System.Collections.Generic;

namespace login_api.Data
{
    public partial class Role
    {
        public Role()
        {
            UserLogins = new HashSet<UserLogin>();
        }

        public decimal RoleId { get; set; }
        public string RoleName { get; set; } = null!;

        public virtual ICollection<UserLogin> UserLogins { get; set; }
    }
}
