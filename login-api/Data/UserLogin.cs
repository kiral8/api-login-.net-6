﻿using System;
using System.Collections.Generic;

namespace login_api.Data
{
    public partial class UserLogin
    {
        public UserLogin()
        {
            RefreshTokens = new HashSet<RefreshToken>();
        }

        public decimal UserId { get; set; }
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public decimal RoleId { get; set; }

        public virtual Role Role { get; set; } = null!;
        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
