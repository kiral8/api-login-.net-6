﻿using System;
using System.Collections.Generic;

namespace login_api.Data
{
    public partial class RefreshToken
    {
        public decimal RtokenId { get; set; }
        public string Token { get; set; } = null!;
        public string JwtId { get; set; } = null!;
        public decimal UserId { get; set; }

        public virtual UserLogin User { get; set; } = null!;
    }
}
