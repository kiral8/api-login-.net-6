﻿using System;
using System.Collections.Generic;

namespace login_api.Data
{
    public partial class Person
    {
        public decimal PersonId { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
    }
}
