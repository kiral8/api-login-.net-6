﻿using AutoMapper;
using login_api.Data;
using login_api.Models.Dto.Request;
using login_api.Models.Dto.Response;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace login_api.Services
{
    public class TokenService
    {
        private IConfiguration _configuration;
        private readonly ModelContext _context;
        private readonly TokenValidationParameters _tokenValidationParams;

        public TokenService(IConfiguration configuration, TokenValidationParameters tokenValidationParams, ModelContext context)
        {
            _configuration = configuration;
            _context = context;
            _tokenValidationParams = tokenValidationParams;
        }

        public async Task<LoginDto> GenerateToken(UserLogin user)
        {
            // Clase con métodos para generar el JWT
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            // Se obtienen las configuraciones del JWT
            var jwtSettings = _configuration.GetSection("JwtSettings");
            // Secreto para firmar Token
            string secretKey = jwtSettings.GetValue<string>("SecretKey");
            // Tiempo de expiración del Token
            int minutes = jwtSettings.GetValue<int>("MinuteToExpiration");

            var key = Encoding.ASCII.GetBytes(secretKey);

            // Información (payload) que será contenida dentro del Token 
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("Id", user.UserId.ToString()),
                    new Claim(JwtRegisteredClaimNames.Name, user.Username),
                    new Claim(ClaimTypes.Role, user.Role.RoleName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(minutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            // Creación del Token
            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);

            // Creación de Refresh Token
            var refreshToken = new RefreshToken()
            {
                Token = RandomString(35) + Guid.NewGuid(),
                JwtId = token.Id,
                UserId = user.UserId
            };

            await _context.RefreshTokens.AddAsync(refreshToken);
            await _context.SaveChangesAsync();

            LoginDto loginDto = new LoginDto()
            {
                Token = jwtToken,
                RefreshToken = refreshToken.Token
            };

            return loginDto;
        }

        public async Task<GlobalResponse<LoginDto>> RefreshToken(TokenRequestDto tokenRequestDto)
        {
            JwtSecurityTokenHandler jwtTokenHandler = new JwtSecurityTokenHandler();

            GlobalResponse<LoginDto> response = new GlobalResponse<LoginDto>();
            try
            {
                // Validación 1 - Validar formato JWT Token
                ClaimsPrincipal tokenInVerification = jwtTokenHandler.ValidateToken(tokenRequestDto.Token, _tokenValidationParams, out var validatedToken);

                // Validación 2 - Validar algoritmo de encriptación
                if (validatedToken is JwtSecurityToken jwtSecurityToken)
                {
                    bool result = jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase);

                    if (result == false)
                    {
                        response.status = false;
                        response.message = "Token doesn't valid";
                        return response;
                    }
                }

                // Validación 3 - Validar fecha de expiración
                //long utcExpiryDate = long.Parse(tokenInVerification.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Exp).Value);

                //DateTime expiryDate = UnixTimeStampToDateTime(utcExpiryDate);

                //if (expiryDate > DateTime.UtcNow)
                //{
                //    response.status = false;
                //    response.message = "Token has not yet expired";
                //    return response;
                //}

                // Validación 4 - Validar existencia del token
                var storedToken = await _context.RefreshTokens.FirstOrDefaultAsync(x => x.Token == tokenRequestDto.RefreshToken);

                if (storedToken == null)
                {
                    response.status = false;
                    response.message = "Token does not exist";
                    return response;
                }

                // Validación 5 - Validar el ID
                var jti = tokenInVerification.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

                if (storedToken.JwtId != jti)
                {
                    response.status = false;
                    response.message = "Token doesn't match";
                    return response;
                }

                // Se elimina refreshToken
                _context.RefreshTokens.Remove(storedToken);
                await _context.SaveChangesAsync();

                // Buscar usuario para generar nuevo token y nuevo refreshToken
                var userDb = await _context.UserLogins.Include(x => x.Role).SingleOrDefaultAsync(x => x.UserId == storedToken.UserId);

                if (userDb == null)
                {
                    response.status = false;
                    response.message = "Something went wrong with user.";
                    return response;
                }

                // Se genera nuevo token y nuevo refreshToken
                LoginDto logindto = await GenerateToken(userDb);

                response.status = true;
                response.message = "Refresh Token successfully!";
                response.data = logindto;
                return response;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Lifetime validation failed. The token is expired."))
                {

                    response.status = false;
                    response.message = "Token has expired please re-login";
                    return response;

                }
                else
                {
                    response.status = false;
                    response.message = "Something went wrong.";
                    return response;
                }
            }
        }

        private DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            var dateTimeVal = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTimeVal = dateTimeVal.AddSeconds(unixTimeStamp).ToUniversalTime();

            return dateTimeVal;
        }

        private string RandomString(int length)
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(x => x[random.Next(x.Length)]).ToArray());
        }
    
        public async void checkAndDeleteRefreshToken(decimal user_id)
        {
            var refreshToken = await _context.RefreshTokens.SingleOrDefaultAsync(x => x.UserId == user_id);

            if (refreshToken != null)
            {
                _context.RefreshTokens.Remove(refreshToken);
                await _context.SaveChangesAsync();
            }
            
        }
    }
}
