﻿using AutoMapper;
using login_api.Data;
using login_api.Dto.Request;
using login_api.Models.Dto.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Dynamic;

namespace login_api.Services
{
    public class UserService
    {
        private readonly ModelContext _context;
        private readonly IPasswordHasher<UserLogin> _passwordHasher;
        private readonly IMapper _mapper;

        public UserService(ModelContext context, IPasswordHasher<UserLogin> passwordHasher, IMapper mapper)
        {
            _context = context;
            _passwordHasher = passwordHasher;
            _mapper = mapper;
        }

        public async Task<GlobalResponse<UserDto>> CreateUser(UserCreateDto userCreateDto)
        {
            GlobalResponse<UserDto> response = new GlobalResponse<UserDto>();
            try
            {
                // Validar si rol existe
                bool roleExist = await _context.Roles.AnyAsync(x => x.RoleId == userCreateDto.RoleId);

                if (!roleExist)
                {
                    response.status = false;
                    response.message = "role not exist";
                    return response;
                }

                // Validar si username existe
                bool searchedUser = await _context.UserLogins.AnyAsync(user => user.Username == userCreateDto.Username);

                if (searchedUser)
                {
                    response.status = false;
                    response.message = "username is already in use";
                    return response;
                }

                // Se mapea el dto a la entidad
                UserLogin userEntity = _mapper.Map<UserLogin>(userCreateDto);
                userEntity.Password = _passwordHasher.HashPassword(userEntity, userEntity.Password);

                // Se crea usuario en la DB
                EntityEntry<UserLogin> userCreated = _context.UserLogins.Add(userEntity);
                await _context.SaveChangesAsync();

                // Se obtiene usuario creado junto a su rol (Se hace así porque nos interesa obtener el nombre del rol)
                var userWithRole = await _context.UserLogins.Include(x => x.Role).SingleOrDefaultAsync(u => u.UserId == userCreated.Entity.UserId);

                // Se mapea entidad a dto
                UserDto userDto = _mapper.Map<UserDto>(userWithRole);

                // Se estructura respuesta de salida
                response.status = true;
                response.message = "User " + userDto.Username + " created successfully";
                response.data = userDto;
            }
            catch (Exception ex)
            {
                response.status = false;
                response.message = ex.Message;
            }
            return response;

        } 
    
        public async Task<UserLogin?> GetUserByUsername(string username)
        {
            try
            {
                var userDb = await _context.UserLogins.Include(x => x.Role).SingleOrDefaultAsync(u => u.Username == username);
                return userDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
