﻿using login_api.Data;
using login_api.Models.Dto.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;

namespace login_api.Filters
{
    public class ValidSessionTokenFilter : IAsyncActionFilter
    {
        private readonly ModelContext _context;

        public ValidSessionTokenFilter(ModelContext context)
        {
            _context = context;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            response.message = "Token doesn't valid";

            // Buscamos el claim jti dentro de la autenticación del usuario
            var jti = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti);

            if (jti == null)
            {
                context.Result = new UnauthorizedObjectResult(response);
                return;
            }

            // Validamos que aún tenga una sesión al Token enviado
            var refresh = await _context.RefreshTokens.SingleOrDefaultAsync(x => x.JwtId == jti.Value);

            if (refresh == null)
            {
                context.Result = new UnauthorizedObjectResult(response);
                return;
            }

            await next();
        }
    }
}
