﻿using login_api.Data;
using login_api.Filters;
using login_api.Models.Dto.Request;
using login_api.Models.Dto.Response;
using login_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace login_api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly TokenService _tokenService;
        private readonly IPasswordHasher<UserLogin> _passwordHasher;

        public AuthController(UserService userService, IPasswordHasher<UserLogin> passwordHasher, TokenService tokenService)
        {
            _userService = userService;
            _passwordHasher = passwordHasher;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("login")]
        [ProducesResponseType(typeof(GlobalResponse<LoginDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Login([FromBody] UserLoginDto userLogin)
        {
            GlobalResponse<LoginDto> response = new GlobalResponse<LoginDto>();
            try
            {
                // Consultar si es que existe el usuario
                var user = await _userService.GetUserByUsername(userLogin.username);

                if (user == null)
                {
                    response.status = false;
                    response.message = "Wrong username or password";
                    return Ok(response);
                }

                // Validar password
                var verifyPassword = _passwordHasher.VerifyHashedPassword(user, user.Password, userLogin.password);

                if (verifyPassword != PasswordVerificationResult.Success)
                {
                    response.status = false;
                    response.message = "Wrong username or password";
                    return Ok(response);
                }

                // Valida si existe una sesión y la elimina (refreshToken!)
                _tokenService.checkAndDeleteRefreshToken(user.UserId);

                // Creación de Token
                LoginDto logindto = await _tokenService.GenerateToken(user);

                response.status = true;
                response.message = "Login successfully!";
                response.data = logindto;

                return Ok(response);
            }
            catch (Exception ex)
            {
                response.status = false;
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
            
        }
    
        [HttpPost]
        [Route("refreshToken")]
        [Authorize]
        [ProducesResponseType(typeof(GlobalResponse<LoginDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> RefreshToken([FromBody] TokenRequestDto tokenRequestDto)
        {
            var tokens = await _tokenService.RefreshToken(tokenRequestDto);
            return Ok(tokens);
        }
    }
}
