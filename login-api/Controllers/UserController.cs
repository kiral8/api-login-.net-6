﻿using login_api.Dto.Request;
using login_api.Filters;
using login_api.Models.Dto.Response;
using login_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace login_api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(GlobalResponse<UserDto>), StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateUser([FromBody] UserCreateDto userCreateDto)
        {
            GlobalResponse<UserDto> response = await _userService.CreateUser(userCreateDto);

            if (response.status)
            {
                return Created(nameof(CreateUser), response);
            } 
            else
            {
                return BadRequest(response);
            }
        }

        [HttpGet]
        [Route("Test")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [ServiceFilter(typeof(ValidSessionTokenFilter))]
        public string test()
        {
            return "Prueba correcta";
        }
    }
}
