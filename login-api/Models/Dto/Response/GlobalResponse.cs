﻿namespace login_api.Models.Dto.Response
{
    public class GlobalResponse<T>
    {
        public bool status { get; set; }
        public string message { get; set; } = string.Empty;
        public T? data { get; set; }
    }
}
