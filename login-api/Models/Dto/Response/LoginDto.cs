﻿namespace login_api.Models.Dto.Response
{
    public class LoginDto
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; } = string.Empty;
    }
}
