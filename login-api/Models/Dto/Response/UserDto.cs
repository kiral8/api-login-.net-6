﻿namespace login_api.Models.Dto.Response
{
    public class UserDto
    {
        public int UserId { get; set; }
        public string Username { get; set; } = null!;
        public string RoleName { get; set; } = "";
    }
}
