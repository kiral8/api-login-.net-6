﻿using AutoMapper;
using login_api.Data;
using login_api.Dto.Request;
using login_api.Models.Dto.Response;

namespace login_api.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserCreateDto, UserLogin>().ReverseMap();

            CreateMap<UserLogin, UserDto>().IncludeMembers(user => user.Role);
            CreateMap<Role, UserDto>();
        }
    }
}
